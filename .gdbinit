set confirm off
set breakpoint pending on

python
# prompt

def prompt(old):
    from os import environ
    from subprocess import check_output
    try:
        environ["PROMPT_SHELL"] = "gdb" #+ gdb.VERSION
        return check_output(["prompt"]).decode("utf8")
    except Exception:
        return "(gdb) "
gdb.prompt_hook = prompt
del prompt

# pretty printers

def type_pretty_printer(f):
    class TypePrettyPrinter:
        def __init__(self, val):
            self.val = val
        def display_hint(self):
            return self.__class__.__name__
    return type(f.__name__, (TypePrettyPrinter,), {"to_string": (lambda self: f(self.val))})

class TypesPrettyPrinter:
    def __init__(self):
        self.name = self.__class__.__name__
        self.enabled = True
    def __call__(self, val):
        t = gdb.types.get_basic_type(val.type).tag
        if not t: t = val.type.name
        if t:
            f = getattr(self, t, None)
            if f and isinstance(f, type):
                return f(val)

class GamePrettyPrinter(TypesPrettyPrinter):
    @type_pretty_printer
    def Material(val):
        def getsym(name):
            s, b = gdb.lookup_symbol(name)
            if s: return int(s.value().address)
        knownmats = {getsym(n + "_load"): n for n in ["solid", "phong", "pbr"]}
        type = knownmats.get(int(val["load"]), None)
        if type:
            paramstype = gdb.lookup_type("struct " + {"solid": "Solid", "phong": "Phong", "pbr": "PBR"}[type] + "MaterialParams").pointer()
            params = val["params"].cast(paramstype).dereference()
        else:
            params = val["params"]
        return "{" + ", ".join([
            "program=" + str(val["program"]),
            "polygonMode=" + str(val["polygonMode"]),
            (("\"" + type + "\"") if type else ("load=" + str(val["load"]))),
            "params="+ str(params)
        ]) + "}"

    nodetypes = ["empty", "geometry", "dlight", "plight", "camera", "bone"]

    @type_pretty_printer
    def Node(val, _types=nodetypes):
        try: t = _types[int(val["type"])]
        except: return "invalid node type"
        return "node " + (("\"" + val["name"].string() + "\" ") if val["name"] else "") + "{" \
          + "type=" + t \
          + ", father=" + str(val["father"]) \
          + ", children=[" + ", ".join([str(val["children"][i]) for i in range(int(val["nbChildren"]))]) + "]" \
          + ", position=" + str(val["position"]) \
          + ", orientation=" + str(val["orientation"]) \
          + ", scale=" + str(val["scale"]) \
          + ((", data." + t + "=" + str(val["data"][t])) if t in map(lambda f: f.name, val["data"].type.fields()) else "") \
          + ((", alwaysDraw=" + str(val["alwaysDraw"])) if int(val["alwaysDraw"]) else "") \
          + "}"

    @staticmethod
    def node_tree(val, _types=nodetypes):
        if val.type.tag is None:
            val = val.dereference()
        if val.type.tag != "Node":
            print("Not a node.")
            return
        stack = [(val, "")]
        while stack:
            val, indent = stack.pop()
            try: t = _types[int(val["type"])]
            except: t = "invalid"
            print(indent + t + " node " + (("\"" + val["name"].string() + "\" ") if val["name"] else "") + "at " + str(val.address))
            n = int(val["nbChildren"])
            indent = "".join(map(lambda c: {"├": "│", "└": " ", "─": " "}.get(c, c), indent))
            for i in range(n):
                stack.append((val["children"][n - 1 - i].dereference(), indent + ("├─" if i else "└─")))

    def _mat_param(val):
        try: t = ["constant", "shared", "texture"][int(val["mode"])]
        except: return "invalid"
        return t + ":" + str(val["value"][t])

    @type_pretty_printer
    def MatParamFloat(val, _mat_param=_mat_param):
        return _mat_param(val)

    @type_pretty_printer
    def MatParamVec3(val, _mat_param=_mat_param):
        return _mat_param(val)

    @type_pretty_printer
    def AlphaParams(val):
        try: t = ["disabled", "blend", "test"][int(val["mode"])]
        except: return "invalid"
        if int(val["mode"]):
            t += ":" + str(val["alpha"])
        return t

game = GamePrettyPrinter()
gdb.printing.register_pretty_printer(None, game)
del GamePrettyPrinter

class SL3DPrettyPrinter(TypesPrettyPrinter):
    @type_pretty_printer
    def Dog(val):
        pulledType = {1: "Dog", 2: "Sled"}.get(int(val["pullType"]))
        pulledObject = val["pulledObject"]
        if pulledType is not None:
            pulledObject = pulledObject.cast(gdb.lookup_type("struct " + pulledType).pointer()).dereference()
        return "Dog(" + ", ".join([
            str(val["status"]),
            "pos=" + str(val["nodes"]["position"]),
            "orient=" + str(val["nodes"]["orientation"]),
            str(val["nodes"]["changedFlags"]),
            "traction=" + str(val["traction"]),
            "goal=" + str(val["goal"]),
            "forceNorm=" + str(val["forceNorm"]),
            "\n\tpulledObject=" + str(pulledObject)
        ]) + ")"

    @type_pretty_printer
    def Sled(val):
        return "Sled(" + ", ".join([
            "pos=" + str(val["node"]["position"]),
            "orient=" + str(val["node"]["orientation"]),
            str(val["node"]["changedFlags"]),
            "speed=" + str(val["speed"]),
            "angularSpeed=" + str(val["angularSpeed"]),
            "traction=" + str(val["traction"]),
            "brake=" + str(val["brake"]),
            "isFlying=" + str(val["isFlying"]),
            "isGliding=" + str(val["isGliding"]),
        ]) + ")"

gdb.printing.register_pretty_printer(None, SL3DPrettyPrinter())
del SL3DPrettyPrinter

del TypesPrettyPrinter
del type_pretty_printer

end

define game_node_tree
python game.node_tree(gdb.parse_and_eval("$arg0"))
end
