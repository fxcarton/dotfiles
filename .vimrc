syntax on
set background=dark
if has("autocmd")
  filetype plugin indent on
endif
set expandtab ts=4 sw=4 sts=-1 ai
set showmatch
set updatetime=250

set mouse=a
if matchstr($TERM, "^st") is# "st"
    set ttymouse=sgr
    " Fix <S-Tab>
    exe "set t_kB=" . nr2char(27) . "[Z"
else
    set ttymouse=xterm
endif
if &term is# 'linux'
	set t_Co=256
endif

" Disable crap
set nohlsearch
set modelines=0
set nomodeline

" Put files in .vim/
set viminfo+=n~/.vim/info
set directory=.,~/.vim/swap
set backupdir=.,~/.vim/backup
set undodir=.,~/.vim/undo
for s:dir in [expand(&directory), expand(&backupdir), expand(&undodir)]
    let s:dir = substitute(s:dir, "^\.,", "", "")
    if !isdirectory(s:dir)
        call mkdir(s:dir)
    endif
endfor

set wildignore+=*.o,*.aux
set title titlestring=vim\ -\ %f%M%R

au BufNewFile,BufRead *.h set ft=c
au FileType sh let b:is_bash=1

" Autoload saved view if exists
autocmd BufRead * if &ft != "qf" | silent loadview | endif

" Don't close folds when setting foldmethod
set foldlevelstart=99

" In diff mode, automatically update the differences upon writing changes
autocmd BufWritePost * if &diff == 1 | diffupdate | endif
