function! FXC_foldexpr_email(line)
    let l:n = 0
    let l:line = a:line
    while match(l:line, "^> *") == 0
        let l:n += 1
        let l:line = substitute(l:line, "^> *", "", "")
    endwhile
    if match(l:line, "^On .* wrote:$") == 0 || match(l:line, "^Le .* a écrit[\UA0 ]:$") == 0
        let l:n += 1
    endif
    return l:n
endfunction
function! s:fold_email()
    setlocal foldlevel=0
    setlocal foldexpr=FXC_foldexpr_email(getline(v:lnum))
    setlocal foldmethod=expr
endfunction
au FileType mail call s:fold_email()
