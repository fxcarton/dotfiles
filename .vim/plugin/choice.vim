function! choice#choice(input, ...)
    let l:input = a:input
    let l:largs = a:000
    let l:args = ""

    if a:0 == 1 && type(a:1) == v:t_list
        let l:largs = a:1
    endif
    if type(l:input) == v:t_list
        let l:input = join(l:input, "\n") . "\n"
    elseif type(l:input) == v:t_dict
        let l:sep = ":"
        let l:next = 0
        for l:arg in l:largs
            if l:arg is# "-s"
                let l:next = 1
            elseif l:next
                let l:sep = l:arg
                let l:next = 0
            else
                let l:next = 0
            endif
        endfor
        let l:args = " -s " . shellescape(l:sep)
        let l:input = ""
        for [l:k, l:v] in items(a:input)
            let l:input = l:input . l:k . l:sep . l:v . "\n"
        endfor
    endif
    for l:arg in l:largs
        let l:args = l:args . " " . shellescape(l:arg)
    endfor

    silent let l:r = system("choice" . l:args, l:input)
    redraw!
    return l:r
endfunction

function! choice#grep(...)
    let l:largs = a:000
    let l:args = ""
    if a:0 == 1 && type(a:1) == v:t_list
        let l:largs = a:1
    endif
    for l:arg in l:largs
        let l:args = l:args . " " . shellescape(l:arg)
    endfor
    let l:args = l:args . " -Hn"
    let l:entries = split(system("grep" . l:args), "\n")
    call map(l:entries, {k, v -> substitute(v, ":", "+", "")})
    let l:r = split(choice#choice(l:entries, "-s", ":", "-d", "\x1B[32m%k\x1B[39m %v"), "+", 1)
    let l:n = remove(l:r, -1)
    return [join(l:r, "+"), l:n]
endfunction

function! choice#vimgrep(cmd, ...)
    let [l:f, l:n] = call("choice#grep", a:000)
    execute(":" . a:cmd . " " . l:f)
    call cursor(l:n, 1)
endfunction

function! choice#egrep(...)
    call call("choice#vimgrep", ["edit"] + a:000)
endfunction

function! choice#hgrep(...)
    call call("choice#vimgrep", ["split"] + a:000)
endfunction

function! choice#vgrep(...)
    call call("choice#vimgrep", ["vsplit"] + a:000)
endfunction

function! s:arg_or_cword(...)
    if len(a:000) > 0
        return a:1
    endif
    return expand("<cword>")
endfunction

command! -nargs=? ChoiceGrep call choice#egrep(s:arg_or_cword(<f-args>), "-r", ".")
command! -nargs=? ChoiceHgrep call choice#hgrep(s:arg_or_cword(<f-args>), "-r", ".")
command! -nargs=? ChoiceVgrep call choice#vgrep(s:arg_or_cword(<f-args>), "-r", ".")
