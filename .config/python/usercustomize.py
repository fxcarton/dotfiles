def get_prog():
    from os import getpid, readlink
    return readlink("/proc/%d/exe" % getpid())
try:
    PROG = get_prog()
except:
    PROG = None
del get_prog

class PS1:
    from os import environ
    from sys import version_info
    def __init__(self):
        self.e = None
        self.et = ""
        self.etb = None
        if PROG is None:
            self.prog = "python" + str(self.version_info[0])
        else:
            from os.path import basename
            self.prog = basename(PROG)
    def __str__(self):
        try:
            from subprocess import check_output
            self.environ["PROMPT_SHELL"] = self.prog
            self.environ["PROMPT_STATUS"] = self.et
            self.et = ""
            p = check_output(["prompt"])
            if self.version_info[0] >= 3:
                p = p.decode("utf8")
            return p
        except Exception:
            return ">>> "
    def excepthook(self, exctype, value, traceback):
        self.et = exctype.__name__
        self.e = value
        self.etb = traceback
    def traceback(self):
        from traceback import extract_tb, format_list
        def init(self, ps1):
            self.ps1 = ps1
        def repr(self):
            if self.ps1.etb is None:
                return "None"
            return "Traceback (most recent call last):\n" + "".join(format_list(extract_tb(self.ps1.etb))) + self.ps1.e.__class__.__name__ + ": " + str(self.ps1.e)
        return type("TB", tuple(), {"__init__": init, "__repr__": repr})(self)

import sys
sys.ps1 = PS1()
del sys
del PS1

def set_interactive_hook():
    import sys
    prevhook = getattr(sys, "__interactivehook__", None)
    def hook(*a, **k):
        if prevhook is not None:
            prevhook(*a, **k)
        sys.excepthook = sys.ps1.excepthook
        import builtins
        builtins.tb = builtins.traceback = sys.ps1.traceback()

        def history():
            from readline import get_current_history_length, get_history_item, add_history, clear_history
            from subprocess import Popen, PIPE, DEVNULL
            from fcntl import ioctl
            from termios import tcgetattr, tcsetattr, ECHO, TCSADRAIN
            h = [get_history_item(i) for i in range(1, get_current_history_length() + 1)]
            if len(h) > 0 and h[-1] == "__h()":
                del h[-1]
                # this is retarded, readline should provide a way to delete last entry without clearing and readding everything...
                clear_history()
                for l in h:
                    add_history(l)
            choice = Popen(["choice", "-e", "-1", "-s", "", "-r", "%k"], stdin=PIPE, stdout=PIPE, stderr=DEVNULL)
            out, err = choice.communicate(input=b"\n".join(map(lambda l: l.encode("utf8"), h)))
            if choice.returncode != 0:
                out = b"\x19" # C-y
            elif out[-1] == 0xA:
                out = out[:-1]
            with open("/dev/tty") as f:
                fd = f.fileno()
                oldtermattr = tcgetattr(fd)
                newtermattr = oldtermattr.copy()
                newtermattr[3] = newtermattr[3] & ~ECHO
                try:
                    tcsetattr(fd, TCSADRAIN, newtermattr)
                    for b in out:
                        ioctl(fd, 0x5412, bytes((b,)))
                finally:
                    tcsetattr(fd, TCSADRAIN, oldtermattr)
        builtins.__h = history
        from readline import parse_and_bind
        parse_and_bind("\"\\C-e\":end-of-line") # not present in vi mode, needed for C-r
        parse_and_bind("\"\\C-r\":\"\\C-e\\C-u__h()\\n\"")

    sys.__interactivehook__ = hook
set_interactive_hook()
del set_interactive_hook

import pdb
class Pdb(pdb.Pdb, object):
    def __init__(self, *args, **kwargs):
        from subprocess import check_output
        from os import environ
        from sys import version_info
        super(self.__class__, self).__init__(*args, **kwargs)
        try:
            environ["PROMPT_SHELL"] = "pdb" + str(version_info[0])
            p = check_output(["prompt"])
            if version_info[0] >= 3:
                p = p.decode("utf8")
            self.prompt = p
        except:
            pass
pdb.Pdb = Pdb
del Pdb
del pdb

def history():
    from os import getuid, environ
    from os.path import join as path_join, exists as path_exists
    from pwd import getpwuid
    from readline import read_history_file, write_history_file
    from atexit import register as atexit_register
    if PROG is None:
        prog = "python"
    else:
        from os.path import basename
        prog = basename(PROG)
    try:
        path = path_join(environ["XDG_CACHE_HOME"], prog + "_history")
        path = path_join(getpwuid(getuid()).pw_dir, ".cache", prog + "_history")
    except:
        return
    if path_exists(path):
        read_history_file(path)
    atexit_register(lambda: write_history_file(path))
    return path
if not PROG.endswith("gdb"):
    history()
del history

def numpy_help():
    from numpy import ufunc, info
    from builtins import help as builtin_help
    class Help:
        def __call__(self, *args, **kwargs):
            if len(args) == 1 and isinstance(args[0], ufunc):
                return info(*args, **kwargs)
            return builtin_help(*args, **kwargs)
        def __repr__(self):
            return repr(builtin_help)
        def __str__(self):
            return str(builtin_help)
    import builtins
    builtins.help = Help()
try:
    numpy_help()
except:
    pass
del numpy_help
