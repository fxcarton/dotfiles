XAUTHORITY="$HOME/.cache/.Xauthority"; export XAUTHORITY
GPG_TTY="`tty`"; export GPG_TTY
SSH_AUTH_SOCK="`gpgconf --list-dirs agent-ssh-socket`"; export SSH_AUTH_SOCK
gpgconf --launch gpg-agent

QT_STYLE_OVERRIDE="adwaita-dark"; export QT_STYLE_OVERRIDE
GTK_THEME="Adwaita:dark"; export GTK_THEME

shopt -s histappend
HISTCONTROL=ignoreboth
HISTSIZE=-1
HISTFILESIZE=-1
HISTFILE="$HOME/.cache/bash_history"
LESSHISTFILE="$HOME/.cache/lesshst"; export LESSHISTFILE

# If not running interactively, don't do anything
case "$-" in *i*);; *) return;; esac

# Misc options
shopt -s checkwinsize
set -o vi
EDITOR=vim; export EDITOR
MANSECT=2,2p,3,3p,1,1p,0,0p,4,5,6,7,8,9,tcl,n,l,p,o,1x,2x,3x,4x,5x,6x,7x,8x; export MANSECT
LESS="-FR"; export LESS

# XDG dirs
XDG_DATA_HOME="$HOME/.local/share"; export XDG_DATA_HOME
XDG_CONFIG_HOME="$HOME/.config"; export XDG_CONFIG_HOME
XDG_DATA_DIRS="/usr/share/"; export XDG_DATA_DIRS
XDG_CONFIG_DIRS="/etc/xdg"; export XDG_CONFIG_DIRS
XDG_CACHE_HOME="$HOME/.cache"; export XDG_CACHE_HOME

NCFTPDIR="$XDG_CONFIG_HOME/ncftp"; export NCFTPDIR
ELINKS_CONFDIR="$XDG_CONFIG_HOME/elinks"; export ELINKS_CONFDIR
WGETRC="$XDG_CONFIG_HOME/wgetrc"; export WGETRC
SFEEDRC="$XDG_CONFIG_HOME/sfeedrc"; export SFEEDRC

# Steam
STEAM_RUNTIME=1; export STEAM_RUNTIME
STEAM_RUNTIME_PREFER_HOST_LIBRARIES=1; export STEAM_RUNTIME_PREFER_HOST_LIBRARIES
#export LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+${LD_LIBRARY_PATH}:}/usr/lib32/apulse"

# local
PATH="$HOME/.local/bin:$HOME/.local/usr/bin:$PATH"
PKG_CONFIG_PATH="${PKG_CONFIG_PATH:+${PKG_CONFIG_PATH}:}$HOME/.local/lib/pkgconfig:$HOME/.local/usr/lib64/pkgconfig"; export PKG_CONFIG_PATH
LD_LIBRARY_PATH="$HOME/.local/lib:$HOME/.local/usr/lib64"; export LD_LIBRARY_PATH
test -z "$MANPATH" && command -v manpath > /dev/null 2>&1 && MANPATH="`manpath`"
MANPATH="${MANPATH:+${MANPATH}:}$HOME/.local/share/man:$HOME/.local/usr/share/man"; export MANPATH

# nds & 3ds dev
PATH="$PATH:$HOME/dev/aa/trad/tools:$HOME/dev/aa/trad/makefiles"

# cuda
CUDA_HOME="/opt/cuda"
if [ -d "$CUDA_HOME" ]; then
    export CUDA_HOME
    PATH="$PATH:$CUDA_HOME/bin"
    LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+${LD_LIBRARY_PATH}:}$CUDA_HOME/lib64"
else
    unset CUDA_HOME
fi

# R
if command -v R > /dev/null 2>&1; then
    R_LIBS_USER="$HOME/.local/lib/R/`gcc -dumpmachine`/`R --quiet --slave -e 'cat(sprintf("%s.%s", R.version$major, R.version$minor))'`"
    export R_LIBS_USER
fi

# PS1
if command -v prompt > /dev/null 2>&1; then
    PROMPT_FORMAT='\a%r%B?{#PROMPT_STATUS}{%f{1}✘${PROMPT_STATUS} }%f{6}${PROMPT_SHELL} %f{2}%u%r?{SSH_CLIENT}{%B%f{3}}@%h %B%f{4}%d%r%f{2}?{#GIT_DIRTY}{%f{3}}?{GIT_REF}{ ${GIT_REF}}?{GIT_REV}{ ${GIT_REV}}%r\e[K\e]0;${PROMPT_SHELL} - %d\a\n\$ '
    export PROMPT_FORMAT
    PS1='`PROMPT_STATUS="$?" PROMPT_SHELL="\`basename ${SHELL}\`" prompt`'
    PYTHONPATH="$HOME/.config/python${PYTHONPATH:+:${PYTHONPATH}}"
    export PYTHONPATH
fi

# Colors
[ -x /usr/bin/dircolors ] && eval "`dircolors -b`"
LS_COLORS="`printf '%s' "$LS_COLORS" | sed 's/;05//g'`" # disable blink, it's annoying
alias ls='ls --color=auto'
alias grep='grep --color=auto'
GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'; export GCC_COLORS

# Use choice for history
command -v choice > /dev/null 2>&1 && \
bind -x '"\C-r": history|sed "s/^ *[0-9]* *//"|(printf "\\x1b\\x5b\\x34\\x7e\\x15";choice -e -1 -s "" -r "%k" -S "$READLINE_LINE"||printf "%s" "$READLINE_LINE")|perl -e "open(my \$f,\"<\",\"/dev/tty\");while(<>){ioctl(\$f,0x5412,\$_)for split \"\"}"'

# Other useful bindings
for i in {1..9}; do bind -x "\"\\e$i\": fg $i"; done
bind -x "\"\\ej\": jobs"
bind '"\e!": "vim \"$(!!)\"\n"'

# aliases redefining commands
alias python="`command -v python3` -q"
alias gdb="gdb -q"
alias R="R -q --no-save"
vboxsdl() {
    VBOXSDL_CTRL_L="306 64"
    VBOXSDL_CTRL_R="314 0"
    VBOXSDL_SHIFT_L="304 1"
    VBOXSDL_SHIFT_R="303 2"
    VBOXSDL_ALT="308 256"
    VBOXSDL_ALT_GR="313 0"
    VBOXSDL_CTRL_SHIFT_R="314 303 2"
    command vboxsdl --hostkey $VBOXSDL_CTRL_R "$@"
}
ssh() { gpg-connect-agent updatestartuptty /bye >&-; command ssh "$@"; }
scp() { gpg-connect-agent updatestartuptty /bye >&-; command scp "$@"; }
rsync() { gpg-connect-agent updatestartuptty /bye >&-; command rsync "$@"; }
git() { gpg-connect-agent updatestartuptty /bye >&-; command git "$@"; }

# Aliases & commands
alias dump='wget -m -k -x -e robots=off'
alias my_stats='history | awk '\''{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}'\'' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n10'

u() {
    ( [ -z "$1" ] && cat || printf '%s\n' "$1" ) | iconv -f UTF-8 -t UTF-16 | od -tx2 -w2 -v -An | sed 1d | for u in `cat`; do printf '%s ' "$u"; [ "$u" = "000a" ] && printf '\n'; done
}

mcd() {
    mkdir -p "$1" && cd "$1"
}

vimrecov() {
    xargs -0 -a <(find . -name '.*.sw[a-z]' -print0 | sed -z 's:\.\([^/]*\)\.swp:\1:') vim
}

e() {
    "$@" & disown "$!"; exit
}

d() {
    "$@" >/dev/null 2>&1 & disown "$!"
}

readable_asm() {
    gcc -S -O2 -march=native -fno-asynchronous-unwind-tables -fno-stack-protector -o - "$@" | sed '/^[ \t]*.size/d;/^[ \t]*.type/d'
}

m() {
    mutt -f +"$1/Inbox"
}

ml() {
    mutt -f +"local/lists/$1"
}

lmerge() { EPREFIX="$HOME/.local" PORTAGE_CONFIGROOT="$HOME/.local" PORTAGE_OVERRIDE_EPREFIX="" PORTAGE_TMPDIR="$HOME/.local/var/tmp" DISTDIR="$HOME/.local/cache/distfiles" EMERGE_LOG_DIR="$HOME/.local/var/log" FEATURES="-news" emerge "$@"; }
lquery() { ROOT="$HOME/.local" PORTAGE_CONFIGROOT="$HOME/.local" equery "$@"; }

audd() {
    ffmpeg -i - -c:v none -c:a copy -f wav - | curl https://api.audd.io/ -F file=@- | jq .
}

google() {
    elinks "google:$*"
}

if test -n "$BASH_VERSION"; then
    __m_completion() {
        COMPREPLY=( $(compgen -W "$(find "$HOME/mails" -type d -name Inbox -printf '%P\n' | sed 's:/Inbox$::')" -- "${COMP_WORDS[COMP_CWORD]}") )
    }
    complete -F __m_completion m

    __ml_completion() {
        COMPREPLY=( $(compgen -W "$(find "$HOME/mails/local/lists" -mindepth 1 -maxdepth 1 -type d -printf '%P\n')" -- "${COMP_WORDS[COMP_CWORD]}") )
    }
    complete -F __ml_completion ml

    __e_completion() {
        _command_offset 1
        return "$?"
    }
    complete -F __e_completion e d

    __mpop_completion() {
        COMPREPLY=( $(compgen -W "$(mpop --help | sed -n '/^ *-.,/{s/,/\n/;s/^ *//;P;D};s/^ *\(--[^ =[]*=\?\).*/\1/p'; sed -n 's/^account *\(.*\)/\1/p' "$XDG_CONFIG_HOME/mpop/config")" -- "${COMP_WORDS[COMP_CWORD]}") )
    }
    complete -F __mpop_completion mpop
fi

devgit() {
    find ~/dev/ -type d -name .git -exec bash -c 'git -C "$0/.." status | grep -Fq "Your branch is ahead" && sed "s/\\.git\$//" <<< "$0"' {} ';'
}

# Remove non existing dirs in PATH
PATH="`printf '%s' "$PATH" | tr ':' '\0' | xargs -0 -r -n1 sh -c 'test -d "$0" && printf "%s\\0" "$0"' | paste -zsd ':' | tr -d '\0'`"

# If ssh'ed, attach/start tmux
test -n "$SSH_CLIENT" && test "$SHLVL" = 1 && test -z "$TMUX" && command -v tmux >/dev/null 2>&1 && r="$( (tmux list-sessions 2>&-; printf 'n: new session\nr: no tmux\n') | choice -s : )" && case "$r" in
    n) exec tmux;;
    r) ;;
    [0-9]*) exec tmux a -t "$r";;
esac || true
